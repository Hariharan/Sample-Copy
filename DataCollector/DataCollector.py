__author__ = 'naveen.krishnan'

import tornado.ioloop
import tornado.web
try:
    from _version import __version__, __revision__
except ImportError:
    __version__ = "UNKNOWN"
    __revision__ = "UNKNOWN"

class DataCollector(tornado.web.RequestHandler):

    event_data_log_file_name = 'eventdata.log'
    event_data_log_file = ''

    def get(self):
        try:
            event_data_log_file = open(self.event_data_log_file_name, 'r')
            data = str(event_data_log_file.read())
            if data:
                self.write(data)
            else:
                self.write('hdtr fsfNosdfsdfsdf event data available in the System. Please populate event data')
        except IOError as io_error:
            self.write('Error processing sfsfsrequest : ' + str(io_error))
        finally:
            event_data_log_file.close()

    def post(self):
        try:
            event_data_log_file = open(self.event_data_log_file_name, 'a')
            event_data_log_file.write(str(self.request.body))
            self.set_status(202)
        except IOError as io_error:
            self.write('Error procsdfsdsfsfessing request : ' + str(io_error))
        finally:
            event_data_log_file.close()


application = tornado.web.Application([
    (r"/", DataCollector)
])

if __name__ == "__main__":
    port = 8000
    application.listen(port)
    print('Server listeninfsdfsg in port, Use http://<ip_address_of_server>:{0}'.format(str(port)))	
    tornado.ioloop.IOLoop.instance().start()
