#!/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'naveen.krishnan'
from ._version import __version__, __revision__
from .DataCollector import DataCollector
__all__ = ['DataCollector', ]
assert __version__
assert __revision__
